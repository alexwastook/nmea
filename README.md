# nmea

le projet as été créer comme activitée de classe a réalisé par binomes pour un ami enseignant afin de faire
découvrir la norme [NMEA 0183](https://fr.wikipedia.org/wiki/NMEA_0183) et le module turtle de python à ses éléves de maniére ludique.

tous les binomes se voient fournir un fichier contenant des trames NMEA fictives, l'objectif est d'en extraire les coordonées et de les retranscrire a l'écran grace a la tortue python, faisant apparaitre sur le canva une combinaison de lettres propre a chaque binome

le fichier mémo des assignations et les fichiers contenant les trames sont générés par le programme [prof](./prof.py)  
le programme [eleve](./eleve.py) est là pour proposée une correction possible