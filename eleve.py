from turtle import *

def convertion(value):
	parts = value.split(".")
	minutes = int(parts[0])
	secondes = int(parts[1])
	totalEnSecondes = (60 * minutes) + secondes
	return totalEnSecondes
	
try:
	file = open("Binome1.txt")
except(FileNotFoundError):
	print("erreur ligne 10")
	print("Le fichier n'as pas été trouvé :(")
	exit()
		
lines = file.readlines()
file.close()

for line in lines:
	elements = line.split(",")
	latitude = convertion(elements[2])
	longitude = convertion(elements[4])
	goto(latitude,longitude)

exitonclick()