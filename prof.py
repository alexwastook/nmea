import json
from time import sleep, ctime
from random import shuffle, randrange
from itertools import product

alphabet = list("ahnzkx")
keywords = [''.join(i) for i in product(alphabet, repeat = 2)]
shuffle(keywords)

# sert a convertir depuis la base 10 vers la base 60
def to60b(value):
	m = value // 60
	s = value - (60 * m)
	return (
			str(m).zfill(2 if m >= 0 else 3),
			str(s).zfill(2 if s >= 0 else 3)
		)

# sert a convertir depuis la base 60 vers la base 10
def from60b(value):
	return (60 * int(value[0])) + int(value[1])

# sert a méttre en forme les secondes
def formatSec(sec=1):
	values = ctime(3600*3 + sec).split(" ")[4].split(":")
	values.append("."+str(randrange(0,1000)/1000)[2:].zfill(3))
	return "".join(values)


width,height = 400,300

with open("letters.json") as file:
    letters = json.load(file)

basetrame = {
	"entete" : "$GPGGA",
	"temps" : None,
	"latitude" : None,
	"N" : 'N',
	"logitude" : None,
	"E" : 'E',
	"type" : 1,
	"satelites" : "04",
	"precision" : 3.2,
	"altitude" : 200.2,
	"" : 'M, , , ',
	"fin" : "0000*0E"
}

#ahnzkx
binomeNum = 0
assignations = {}
for word in keywords:
	binomeNum += 1
	assignations[("Binome"+str(binomeNum))] = word
	trames = []
	pos = []
	x,y = (0 - width) + 75,0
	pos.append((to60b(x),to60b(y)))
	for letter in word:
		for i,j in eval(letters[letter]):
			x+=i
			y+=j
			pos.append((to60b(x),to60b(y)))
		x+=50
		pos.append(
				(	
					to60b(x),
					to60b(y)
				)
			)
	i = 0
	for lat,lon in pos:
		trame = dict(basetrame)
		trame["latitude"]=".".join(lat)
		trame["logitude"]=".".join(lon)
		i+=1
		trame["temps"]=formatSec(i)
		trames.append(trame)
	
	lines = []
	for i in trames:
		line = ""
		for j in i.values():
			line += (str(j)+",")
		lines.append(line[:-2])
	
	with open(("Binome"+str(binomeNum)+".txt"),'w') as file:
		file.write("\n".join(lines))
		
with open('assignations.json', 'w') as file:
    file.write(json.dumps(assignations, indent=2))